/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;
import java.util.Scanner;
/**
 *
 * @author 23750
 */
public class GuessANumber {
    public static void main(String[] args) {
        
      int random = (int)((int)20 * Math.random());
      int turns = 5;
      
      while (turns != 0) {
          System.out.println("You have " + turns + " turns remaining");
          System.out.println("Guess a number between 0 - 20");
          int pickednumber = new Scanner(System.in).nextInt();
          if (pickednumber == random) { System.out.println("Correct"); break; }
          if (pickednumber > random) { System.out.println("Too Big"); turns = turns - 1; }
          if (pickednumber < random) {System.out.println("Too small"); turns = turns - 1;}
          if (turns == 0) {System.out.println("You ran out of turns");
          
      }
      
      
    }
    
}
}
