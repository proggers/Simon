/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 * 
 * @author 23750
 */

public class Booleanoperators {

    public static void main(String[] args) {

        boolean a = false;
        boolean b = false;

        boolean p = !a; //not
        boolean q = a && b; //and
        boolean r = a || b; //or

        //System.out.println(p);
        //System.out.println(q);
        //System.out.println(r);
        
        boolean z = ( a || b ) && ! ( a && b ); //exclusive or where just one is true
                                                //XOR
        System.out.println(z);
    }

}
