/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23750
 */
public class Radio {
    
    int volume;
    double frequency;
    boolean on;
    
    public Radio (int V, double F, boolean O) {
    volume = V;
    frequency = F;
    on = O;
}
    public void setVolume (int V) {
        volume = V;
    }
    
    public void changeVolumeBy (int d) { 
        volume = volume + d;
    }
    
    public void tuneTo (double F) {
        frequency = F;
    }
    
    public boolean isOn () {
        return on;
    }
    
    public void togglePower () {
    }
    
    public String toString() {
        return "Radio power: " + on + "Volume: " + volume + "frequency: " + 
                frequency;
        
    }
    public static void main(String[] args) {
        
        
    }
}
        
   

