/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23750
 */
public class LoopsPractice {

    public static void main(String[] args) {
        //loop for sum of 1/2 + 2/3 + 3/4 ... to fifteen
        double numerator = 1;
        double denominator = 2;
        double sum = 0;

        for (int loops = 1; loops <= 15; loops++) {

            
            double fraction1 = numerator / denominator;
            
            sum += fraction1;

            numerator += 2;
            denominator += 2;

            
        }
        System.out.println(sum);
    }
}
