/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23750
 */
public class Types {

    public static void main(String[] args) {

        int a = 7; //integer, always rounds off
        double x = 3.14159; //decimal
        String msg = "D.Va is my main"; //holds words (zero or more characters)
        boolean main = true; //true or false
        char c = 'D'; // character (only one)

        double w = 10; //forward slash is to divide
        double y = w / 3;
        System.out.println(y);

        System.out.println(w % 3); // remainder

        System.out.println(Math.pow(x, 2)); //x raised to power 2
        System.out.println(Math.sin(x)); //cos and tan
        System.out.println(Math.sqrt(a)); //square root
        System.out.println(Math.abs(a)); //absolute value (makes positive)
        System.out.println(Math.random()); //gives random number

        double z = Math.sqrt(2);
        System.out.println(z * z);

        while (w > 10) {
            System.out.println(w);
        } // while makes a loop
    }

}
