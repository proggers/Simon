/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23750
 */
public class Polygon {
    int numSides;
    double sideLen;
    
    Polygon(int sides, double length){
        this.numSides = sides;
        this.sideLen = length;
    }
    
    double getArea() {
        double area = sideLen/2*Math.tan(Math.PI/numSides);
        return area;
    }
    
    double getPerimeter() {
        double perimeter = sideLen*4;
        return perimeter;
    }
    
    public String toString() {
        String polygon = numSides + " sided polygon with length " + sideLen;
        return polygon;
    }
    
    public static void main(String[] args) {
        Polygon p = new Polygon(1,2);
        System.out.println(p);
    }
} 

