/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import java.util.Scanner;

/**
 *
 * @author 23750
 */
public class Sequences {

    public static void main(String[] args) {
        while (1 == 1) {
            System.out.println("Enter a positive number");
            int n = new Scanner(System.in).nextInt(); // used for user input

            while (n > 1) {
                System.out.println(n);
                if (n % 2 == 0) {
                    n = n / 2;
                } else {
                    n = n * 3 + 1;
                }
            }
            System.out.println(n);
        }
    }
}
