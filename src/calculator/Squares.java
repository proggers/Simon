///*
package calculator;

/**
 *
 * @author 23750
 */
public class Squares {

    public static void main(String[] args) {
        int N = 1;

        while (N * N < 100) {
            System.out.print("The square of ");
            System.out.print(N);
            System.out.print(" is ");
            System.out.println(N * N);
            N = N + 1;
        }
    }
}
