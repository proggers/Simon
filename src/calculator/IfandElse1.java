/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23750
 */
public class IfandElse1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int NumLegs = 4;
        boolean mammal = true;
        boolean carnivore = false;
        int horns = 2;
        double weight = 1200.5;
        
        if (NumLegs == 4 && horns == 2 && weight > 1000) {
            System.out.println("It is a Giraffe");
        }
        else if (NumLegs == 2 && horns == 0 && weight < 1000) {
            System.out.println("It is a Human");
        }
        else if (NumLegs == 4 && horns == 0 && weight < 1000) {
            System.out.println("It is a Wolverine");
        } 
        else if (NumLegs == 4 && horns == 0 && weight > 1000) {
            System.out.println("It is a Elephant");
        } 
        else if (NumLegs == 4 && horns == 2 && weight < 1000) {
            System.out.println("It is a Komodo Dragon"); 
        }
}
}
    
    

