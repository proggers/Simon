/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import javax.swing.JOptionPane;

/**
 *
 * @author 23750
 */
public class UpperCase {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char ch = (JOptionPane.showInputDialog("Enter a character: ")).charAt(0);

        char c = Character.toUpperCase(ch);
        System.out.println(c);
    }

}
