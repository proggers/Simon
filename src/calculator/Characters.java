/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23750
 */
public class Characters {

    public static void main(String[] args) {

        int i = 15;
        long j = 1L + Integer.MAX_VALUE;
        i = (int) j; //convert long to int
        System.out.println(i);

        char a = ' ';
        System.out.println(a);
        int b = (int) a; //convert char to int
        System.out.println(b);
        
        int weird = 0x04C1;//0x prefix = base 16
        a = (char)weird;
        System.out.println(a);

    }
}
