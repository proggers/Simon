/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23750
 */
public class Factors {

    public static void main(String[] args) {
        prime(21);
        prime(9);
        prime(127);

    }

    public static void prime(int N) {

        int counter = 0;
        int a = 1;
        while (a <= N) {
            if (N % a == 0) {
                //System.out.print(a);
                //System.out.print(" is a factor of ");
                //System.out.println(N);
                counter = counter + 1;
            }
            a = a + 1;
        }
        if (counter == 2) {
            System.out.println((N) + " is a prime number");

        } else {
            System.out.println((N) + " is not a prime number");
        }
    }
}
