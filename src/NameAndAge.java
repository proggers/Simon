/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
import java.io.*;
import javax.swing.*;
/**
 *
 * @author 23750
 */
public class NameAndAge {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)throws IOException {
        
        String fileName = JOptionPane.showInputDialog("Enter file name:");
        
        Scanner scFile = new Scanner (new File(fileName));
        String line = "", name = "";
        int num, sum = 0;
        while(scFile.hasNext()){
            line = scFile.nextLine ();
            Scanner scLine = new Scanner (line).useDelimiter("#");
            name = scLine.next();
            num = scLine.nextInt();
            scLine.close();
            System.out.println(name + " + " + num);
            sum = sum + num;
        }
        scFile.close();
        System.out.println("The sum is " + sum);
    }
    
}
