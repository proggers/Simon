
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javax.swing.JOptionPane;
/**
 *
 * @author 23750
 */
public class ReadNum {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)throws FileNotFoundException {
        
        String fileName = JOptionPane.showInputDialog("Enter file name:");
        
         Scanner ScFile = new Scanner(new File(fileName));
         int num, sum = 0;
        
        while(ScFile.hasNext()){
            num = ScFile.nextInt ();
            System.out.println (num);
            sum = sum + num;
        
    }
    ScFile.close ();
    
    System.out.println ("The sum is " + sum);
}
}